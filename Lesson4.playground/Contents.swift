import Foundation

// --------- первое задание -----------

var firstStr = ""               // создаём новую переменную
firstStr = "Hi"                 // присваиваем новое значение новой переменной
var secondStr = "Hello"         // создаём вторую переменную со значением

if firstStr != secondStr {      // проверяем одинаковые ли значения
    firstStr = secondStr        // присваиваем новое значение
}
debugPrint(firstStr)                 // распесатываем новое знаечение

// -------- второе задание ------------

let a = "qwert"
let b = "qwerty"

if a.count != b.count {
    if a.count > b.count {
        debugPrint(a.first!, a.last!)
    } else {
        debugPrint(b.first!, b.last!)
    }
}

// --------- третье задание -----------

let str = "123456"
for char in stride(from: str.count - 1, through: 0 , by: -1) {
    print(Array(str)[char])
}

//MARK: - ПОВЫШЕННАЯ СЛОЖНОСТЬ

// --------- Первое задание ------------


var line = "Девелопер 123 пошёл кодить"

for char in line {
    if !char.isNumber {
        if let i = line.firstIndex(of: char) {
            line.remove(at: i)
        }
    }
}
debugPrint(line)

// --------- Второе задание ------------

let string = "Мама мыла раму а Саша шла по шосе и сосала сушку"

var arr = string.split(separator: " ").map(String.init)
debugPrint("В строке string \(arr.count) слов")

// --------- Третье задание ------------

arr.sort(by: {$0.count > $1.count})
debugPrint(arr[0])                            // распечатываю самое длинное слово в строке

