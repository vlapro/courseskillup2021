import Foundation


//MARK: - Лёгкий уровень

// ------------- Первое задание -------------

let n = 54123
let array = Array(String(n))

var sum = 0
var prod = 1

for i in array {
    if i.isNumber {
        sum += Int(String(i)) ?? 0
    }
}

for i in array {
    if i.isNumber {
        prod *= Int(String(i)) ?? 0
    }
}

debugPrint("Сумма цифр числа \(n) равно \(sum)")
debugPrint("Произвидение цифр числа \(n) равно \(prod)")
debugPrint()

// ------------- Второе задание -------------

let num = 4
let pow = 2

var res = 1

for _ in 1...pow {
    res *= num
}

debugPrint("Число \(num) в степени \(pow) равно \(res)")

// ------------- Третье задание -------------

let arr: [Float] = [2, 9, 3, 7, 4, 6, 5, 1, 3, 8]
let total = arr.reduce(0, +)
let result = total / Float(arr.count)

debugPrint(result)
debugPrint()

//MARK: - Сложный уровень

// ------------- Первое задание -------------

var arrayInt = [Int]()
for _ in 1...10 {
    arrayInt.append(Int.random(in: 1...100))
}
debugPrint(arrayInt)

var maxElement = arrayInt[0]
for element in stride(from: 0, to: arrayInt.count, by: 2) {
    if arrayInt[element] > maxElement {
        maxElement = arrayInt[element]
    }
}
debugPrint(maxElement)
debugPrint()


// ------------- Второе задание -------------

for i in 0..<arrayInt.count {
    for j in 0..<arrayInt.count - i - 1 {
        if arrayInt[j] > arrayInt[j + 1] {
            arrayInt.swapAt(j + 1, j)
        }
    }
}
debugPrint(arrayInt)
debugPrint()

// ------------- Третье задание -------------

var randomArr = [Int]()
for _ in 1...10 {
    randomArr.append(Int.random(in: 1...100))
}
debugPrint(randomArr)

randomArr.sort()
