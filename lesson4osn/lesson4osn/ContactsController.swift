//
//  ContactsController.swift
//  lesson4osn
//
//  Created by Vladyslav Prosianyk on 06.05.2021.
//

import UIKit

class ContactsController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var contactsTblVw: UITableView!
    
    let cellId = "cellId"
    static var contactsArray: [Contact] = [
        Contact(name: "Марина",
                surname: "",
                number: "+1234567890",
                country: "USA",
                group: .Friend),
        Contact(name: "Василь",
                surname: "Селезняков",
                number: "+7327462921",
                city: "Москва",
                country: "Росія",
                group: .Friend),
        Contact(name: "Мама",
                surname: "💖",
                number: "+380674294354",
                city: "Черкаси",
                country: "Україна",
                group: .Family),
        Contact(name: "Ліза",
                surname: "💝",
                number: "+380674245657",
                city: "Черкаси",
                country: "Україна",
                group: .Family),
        Contact(name: "Отець",
                surname: "Сергій",
                number: "+38063728198",
                city: "Черкаси",
                group: .Church),
        Contact(name: "Начальник",
                number: "+380986324713",
                city: "Київ",
                country: "Україна",
                group: .Work)
    ]
    
    func openInfoVC(contact: Contact) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let infoViewController = storyBoard.instantiateViewController(withIdentifier: "InfoVC") as! InfoViewController
        self.present(infoViewController, animated:true, completion: nil)
        infoViewController.configureWith(contact)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        contactsTblVw.dataSource = self
        contactsTblVw.delegate = self
        
        contactsTblVw.register(UINib(nibName: "ContactTableViewCell", bundle: nil), forCellReuseIdentifier: "ContactCell")
        
        ContactsController.contactsArray.sort(by: {$0.name! < $1.name!})
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        contactsTblVw?.reloadData()
    }
  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ContactsController.contactsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: ContactTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ContactCell") as! ContactTableViewCell
        
        let currentLastItem = ContactsController.contactsArray[indexPath.row]
        cell.configureWith("\(currentLastItem.name ?? "") \(currentLastItem.surname ?? "")", "\(currentLastItem.country ?? "") \(currentLastItem.city ?? "")") {
            self.openInfoVC(contact: currentLastItem)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}

