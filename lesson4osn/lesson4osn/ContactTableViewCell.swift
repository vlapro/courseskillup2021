//
//  ContactTableViewCell.swift
//  lesson4osn
//
//  Created by Vladyslav Prosianyk on 06.05.2021.
//

import UIKit

class ContactTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lContact: UILabel?
    @IBOutlet weak var lCountry: UILabel?
    
    var onClose: (() -> Void)?
        
    @IBAction func btnInfoPressed(_ sender: UIButton) {
       onClose?()
    }
    
    func configureWith(_ contact: String?, _ country: String?, onSelect: @escaping (() -> Void)) {
        onClose = onSelect
        lContact?.text = contact
        lCountry?.text = country
    }
}
