//
//  InfoViewController.swift
//  lesson4osn
//
//  Created by Vladyslav Prosianyk on 07.05.2021.
//

import UIKit

class InfoViewController: UIViewController {
    
    @IBOutlet weak var lNameSurname: UILabel!
    @IBOutlet weak var lPhoneNumber: UITextView!
    @IBOutlet weak var lCityCountry: UITextView!
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func configureWith(_ contact: Contact) {
        lNameSurname?.text = "\(contact.name ?? "") \(contact.surname ?? "")"
        lPhoneNumber?.text = contact.number
        lCityCountry?.text = "\(contact.city ?? "") \(contact.country ?? "")"
    }
}
