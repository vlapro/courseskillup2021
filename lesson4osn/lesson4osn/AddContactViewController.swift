//
//  AddContactViewController.swift
//  lesson4osn
//
//  Created by Vladyslav Prosianyk on 08.05.2021.
//

import UIKit
import Swift

class AddContactViewController: UIViewController {

    
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var tfSurname: UITextField!
    @IBOutlet weak var tfPhone: UITextField!
    @IBOutlet weak var tfCountry: UITextField!
    @IBOutlet weak var tfCity: UITextField!
    @IBOutlet weak var pvGroup: UIPickerView!
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func saveButtonTapped(_ sender: UIButton) {
        if tfPhone.text!.isEmpty {
            let alert = UIAlertController(title: "Ups..", message: "You forgot entered number", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: nil))
            present(alert, animated: true, completion: nil)
        } else {
            let selectedPickerRow = pvGroup.selectedRow(inComponent: 0)
            ContactsController.contactsArray.append(Contact(name: tfName.text ?? "", surname: tfSurname.text ?? "", number: tfPhone.text!, city: tfCity.text ?? "", country: tfCountry.text, group: Group.allCases[selectedPickerRow]))
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pvGroup.dataSource = self
        pvGroup.delegate = self
    }

}

extension AddContactViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        Group.allCases.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return Group.allCases[row].rawValue
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
    }
}
