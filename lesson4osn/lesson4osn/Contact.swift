//
//  Contact.swift
//  lesson4osn
//
//  Created by Vladyslav Prosianyk on 06.05.2021.
//

import Foundation

struct Contact {
    var name: String?
    var surname: String?
    var number: String
    var city: String?
    var country: String?
    var group: Group?
}

public enum Group: String, CaseIterable {
    case Family = "Family"
    case Friend = "Friend"
    case Work = "Work"
    case Church = "Church"
}
