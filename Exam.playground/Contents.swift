import UIKit

class House {
    var numberOfHouse: Int
    var floors: Int
    var flatsOnFloor: Int
    var roomsInFlat: Int
    var entrances: Int
    
    init(numberOfHouse: Int, floors: Int, flatsOnFloor: Int, roomsInFlat: Int, entrances: Int = 1) {
        self.numberOfHouse = numberOfHouse
        self.floors = floors
        self.flatsOnFloor = flatsOnFloor
        self.roomsInFlat = roomsInFlat
        self.entrances = entrances
    }
    
    func printAllInfo() {
        print("""
            Номер будинку: \(numberOfHouse)
            Кількість поверхів: \(floors)
            Клькість квартир на поверсі: \(flatsOnFloor)
            Кількість кімнат в квартирі: \(roomsInFlat)
            """)
    }
    
    func printNumberOfFlatsInHouse() {
        print("Загальна кількість квартир в домі: \(floors * flatsOnFloor * entrances)")
    }
    
    func printNumberOfRoomsInOneEntrance() {
        print("Загальна кількість кімнат в під'їзді: \(floors * flatsOnFloor * roomsInFlat)")
    }
    
    func checkNumberOfFlat(flatNr: Int) {
        let numberOfFlatsInHouse = floors * flatsOnFloor * entrances
        if 1...numberOfFlatsInHouse ~= flatNr {
            print("Квартира номер \(flatNr) знаходиться в данному будинку")
        } else {
            print("Квартира номер \(flatNr) НЕ знаходиться в данному будинку")
        }
    }
    
    func findEntranceAndFloor(flatNr: Int) {
        let numberOfFlatsInHouse = floors * flatsOnFloor * entrances
        let numberOfFlatsInEntrance = floors * flatsOnFloor
        if 1...numberOfFlatsInHouse ~= flatNr {
            let entranceNr = Int((Double(flatNr) / Double(numberOfFlatsInEntrance)).rounded(.up))
            let x = flatNr % numberOfFlatsInEntrance
            let floorNr = Int ((Double(x) / Double(flatsOnFloor)).rounded(.up))
            print("""
                Квартира номер \(flatNr) знаходиться
                в під'їзді номер \(entranceNr),
                на \(floorNr) поверсі.
                """)
        } else {
            print("Квартира номер \(flatNr) НЕ знаходиться в данному будинку")
        }
    }
}

let stalinka = House(numberOfHouse: 1, floors: 9, flatsOnFloor: 4, roomsInFlat: 3, entrances: 8)

stalinka.findEntranceAndFloor(flatNr: 59) 
