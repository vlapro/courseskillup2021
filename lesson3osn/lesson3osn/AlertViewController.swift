//
//  AlertViewController.swift
//  lesson3osn
//
//  Created by Vladyslav Prosianyk on 04.05.2021.
//

import UIKit

class AlertViewController: UIViewController {
    
    var timer = Timer()
    var seconds = 61
    var isTimerRunning = true
    
    @objc func updateTimer() {
        if isTimerRunning {
            seconds -= 1
            timerLbl.text = "У вас залишилось \(seconds) секунд!"
            if seconds == 0 {
                timerLbl.text = "Ну всьо, зараз вас пов'яжуть)"
                isTimerRunning = false
            }
        }
    }
    
    func runTimer() {
         timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(AlertViewController.updateTimer)), userInfo: nil, repeats: true)
    }
    

    @IBOutlet weak var timerLbl: UILabel!
    
    @IBAction func alertButtonTapped(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "УВАГА!", message: "Ви викликали МЧС! Вони прибудуть за 60 секунд!", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Викликати ще й швидку?", style: .destructive, handler: nil))
        alert.addAction(UIAlertAction(title: "Виклакати поліцейских?", style: .destructive, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
        runTimer()
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
}
