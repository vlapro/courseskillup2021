//
//  ColorsViewController.swift
//  lesson3osn
//
//  Created by Vladyslav Prosianyk on 04.05.2021.
//

import UIKit

class ColorsViewController: UIViewController {
        
    @IBAction func buttonPressed(_ sender: UIButton) {
        if sender.titleLabel?.text == "Orange" {
            
            let vc = storyboard?.instantiateViewController(withIdentifier: "OrangeVC")
            self.navigationController?.pushViewController(vc!, animated: true)
            
        } else if sender.titleLabel?.text == "Blue" {
            
            let vc = storyboard?.instantiateViewController(withIdentifier: "BlueVC")
            self.navigationController?.pushViewController(vc!, animated: true)
            
        } else if sender.titleLabel?.text == "Red" {
            
            let vc = storyboard?.instantiateViewController(withIdentifier: "RedVC")
            self.navigationController?.pushViewController(vc!, animated: true)
            
        }
    }


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
