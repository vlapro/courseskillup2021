import Foundation

// Первое задание --------------------------------------------------

let work: String? = "Фрезировщик"
var worker = (name: "Василий", age: 34, meried: true, work: work)
let wifeOfWorker = (name: "Оля", husbent: worker)



// Второе задание --------------------------------------------------

if worker.work != nil && wifeOfWorker.husbent == worker && worker.meried {
    print("\(worker.name) какое-то время назад женился на девушке по имени \(wifeOfWorker.name) и он работает на должности: \(worker.work ?? "Безработный")")
} else {
    print("\(worker.name) - неудачник...")
}

worker.work != nil && wifeOfWorker.husbent == worker && worker.meried ?  print("\(worker.name) какое-то время назад женился на девушке по имени \(wifeOfWorker.name) и он работает на должности: \(worker.work ?? "Безработный")") : print("\(worker.name) - неудачник...")

// Третье задание ---------------------------------------------------


func quadratic(a: Double, b: Double, c: Double) {
    let d = b * b - 4 * a * c
    if d > 0 {
        let x1 = (-b + sqrt(d)) / (2 * a)
        let x2 = (-b - sqrt(d)) / (2 * a)
        print("Если d > 0, то уравнение имеет два различных вещественных корня: x₁ ~ \(x1), x₂ ~ \(x2)")
    } else if d == 0 {
        let x = (-b + sqrt(d)) / (2 * a)
        print("Если d = 0, то уравнение имеет один корень: x ~ \(x)")
    } else {
        print("Если d < 0, то уравнение не имеет вещественных корней.")
    }
}

quadratic(a: -12, b: -4, c: 3)

// Повышенная сложность -----------------------------------------------------------------------------

let s = 427

let s1 = s / 100
let s2 = (s % 100) / 10
let s3 = s % 10

if s1 < s2 && s2 > s3 {
    print(s2)
} else if s1 < s3 {
    print(s3)
} else {
    print(s1)
}

if s1 > s2 && s2 < s3 {
    print(s2)
} else if s1 > s3 {
    print(s3)
} else {
    print(s1)
}



if s1 == s2 || s2 == s3 || s3 == s1 {
    print("Да, есть одинаковые цифры в заданом числе и это:")
    if s1 == s2 {
        print("\(s1) == \(s2)")
    } else if s3 == s2 {
        print("\(s3) == \(s2)")
    } else if s1 == s3 {
        print("\(s1) == \(s3)")
    }
}



let num = 54231
let revers = String(String(num).reversed())

if revers == String(num) {
    print("'num' является полиндромом")
} else {
    print("'num' НЕ является полиндромом")
}
