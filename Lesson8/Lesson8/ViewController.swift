//
//  ViewController.swift
//  Lesson8
//
//  Created by Vladyslav Prosianyk on 22.04.2021.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var loginTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBAction func signInButtonPressed(_ sender: UIButton) {
        let loginText = loginTextField.text
        let passwordText = passwordTextField.text
        
        print("Login: \"\(loginText!)\", password: \"\(passwordText!)\"")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


}

