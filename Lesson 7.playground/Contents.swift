import UIKit

//MARK: - Лёгкий уровень

// ------------ Первое задание ------------

class Tiles {
    var brand: String
    var size_h: Double
    var size_w: Double
    var price: Double
    
    init(brand: String, size_h: Double, size_w: Double, price: Double) {
        self.brand = brand
        self.size_h = size_h
        self.size_w = size_w
        self.price = price
    }
    
    func getData() {
        print("""
            Перодаставляем вашему вниманию кафель бренда \(brand)!
            Его длина: \(size_h), а ширина: \(size_w).
            Ну а цена Вас приятно удевит! Всего \(price) гривен!!!
            Успейте преобрести, этот прекрасный кафель по супер цене!
            """)
        print()
    }
}

var ivanovKafel = Tiles(brand: "Ivanov", size_h: 75.0, size_w: 52.5, price: 119.9)
ivanovKafel.getData()

// ------------ Второе задание ------------

class Student {
    
    let lastname: String
    let fio: String
    var groupNr: Int
    var avarageScore: Double
    
    init(lastname: String, fio: String, groupNr: Int, progres: [Double]) {
        self.lastname = lastname
        self.fio = fio
        self.groupNr = groupNr
        
        let a = Double(progres.count)
        let b = progres.reduce(0, +)
        let avarageScore = b / a
        
        self.avarageScore = avarageScore
    }
    
    class func getStudentsSurnamesWithProgres45(students: [Student]) {
        for student in students where student.avarageScore >= 4 {
            print(student.lastname, student.avarageScore)
        }
    }
}

var piatochkin   = Student(lastname: "П'яточкин", fio: "П.І.С.", groupNr: 123, progres: [3, 4, 5, 4, 5])
var pupkin       = Student(lastname: "Пупкін", fio: "П.Ф.А.", groupNr: 46, progres: [2, 3, 3, 4, 3])
var ivanov       = Student(lastname: "Іванов", fio: "І.В.В.", groupNr: 123, progres: [5, 5, 5, 4, 5])
var shevchenko   = Student(lastname: "Шевченко", fio: "Ш.Т.Г.", groupNr: 43, progres: [3, 4, 4, 4, 3])
var demydenko    = Student(lastname: "Демиденко", fio: "Д.С.Ф.", groupNr: 43, progres: [3, 4, 5, 4, 3])
var kondratiuk   = Student(lastname: "Кондратюк", fio: "К.О.А.", groupNr: 54, progres: [5, 4, 5, 4, 5])
var lysenko      = Student(lastname: "Лисенко", fio: "Л.Е.С.", groupNr: 46, progres: [3, 4, 5, 4, 5])
var pronin       = Student(lastname: "Пронін", fio: "П.П.О.", groupNr: 31, progres: [5, 5, 5, 4, 5])
var chkalov      = Student(lastname: "Чкалов", fio: "Ч.Л.Ф.", groupNr: 31, progres: [3, 4, 4, 4, 5])
var zhdanov      = Student(lastname: "Жданов", fio: "Ж.О.І.", groupNr: 46, progres: [5, 3, 5, 4, 5])

var students: [Student] = [zhdanov, chkalov, pronin, lysenko, kondratiuk, demydenko, shevchenko, ivanov, pupkin, piatochkin]

Student.getStudentsSurnamesWithProgres45(students: students)


//MARK: - Сложный уровень

// ------------ Первое задание ------------

class SomeClass {
    var a: Int = 0 {
        didSet {
            print("Value 'a' has changed from \(oldValue) to \(a)")
        }
    }
    var b: Int = 0 {
        didSet {
            print("Value 'b' has changed from \(oldValue) to \(b)")
        }
    }
    
    func findSum() -> Int {
        return a + b
    }
    
    func findMaxValue() -> Int {
        if a > b {
            return a
        } else {
            return b
        }
    }
}

// ------------ Второе задание ------------

struct Book {
    var name: String
    var autor: String
    var year: Int
}

class HomeLibrary {
    var books: [Book] = []
    
    init(books: [Book]) {
        self.books = books
    }
    
    init() {
    }
    
    func findBookBy(autor: String) {
        for book in books {
            if book.autor.lowercased() == autor.lowercased() {
                print("Книга найдена! \"\(book.name)\" \(book.autor) \(book.year) года")
                return
            }
        }
        print("Книга не найдена..")
    }
    
    func findBookBy(name: String) {
        for book in books {
            if book.name.lowercased() == name.lowercased() {
                print("Книга найдена! \"\(book.name)\" \(book.autor) \(book.year) года")
                return
            }
        }
        print("Книга не найдена..")
    }
    
    func findBookByYear(from: Int, to: Int) {
        for book in books {
            if from...to ~= book.year {
                print("Книга найдена! \"\(book.name)\" \(book.autor) \(book.year) года")
                return
            }
        }
        print("Книга не найдена..")
    }
    
    func sortBy(year: Bool, autor: Bool, name: Bool) {
        if name || year || autor {
            if name {
                books.sort(by: {$0.name > $1.name})
            } else if year {
                books.sort(by: {$0.year > $1.year})
            } else if autor {
                books.sort(by: {$0.autor > $1.autor})
            } else {
                print("Выбрать можно только один пункт")
            }
            for book in books {
                print("\"\(book.name)\" \(book.autor) \(book.year) год")
            }
        } else {
            print("Введить только один запрос 'true' возле предложеных вариантов")
        }
    }
    
    func removeBookWith(name: String?, autor: String?, year: Int?) {
        for index in 0..<books.count {
            if books[index].name == name || books[index].autor == autor || books[index].year == year {
                books.remove(at: index)
            }
        }
    }
    
    func addBook(_ book: Book...) {
        books.append(contentsOf: book)
    }
}

// ------------ Третье задание ------------

