import Foundation
//MARK: - Лёгкий уровень

//---------------- Первое задание ----------------
enum Operatiron {
        case add
        case compute
        case divided
        case multiply
    }

func getResult( params: (first: Int, operation: Operatiron, second: Int )) -> Int {
    let operation = params.operation
    var result = 0
    switch operation {
    case .add:
        result = params.first + params.second
    case .compute:
        result = params.first - params.second
    case .divided:
        result = params.first / params.second
    case .multiply:
        result = params.first * params.second
    }
    return result
}
//---------------- Второе задание ----------------

func printTimeBy(seconds: Int) {
    var runCount = 0
    Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { timer in
        print(Date())
        runCount += 1

        if runCount == seconds {
            timer.invalidate()
            print("Finish!")
        }
    }
   
}

//printTimeBy(seconds: 10)

//MARK: - Сложный уровень

//---------------- Первое задание ----------------

func findPrimeNumberAfter(_ n: Int) {
    func isPrime() -> Bool {
        for i in 2..<n {
            if n % i == 0 {
                return false
            }
        }
        return true
    }
    if isPrime() {
        print("\(n) простое число")
    } else {
        findPrimeNumberAfter(n + 1)
    }
    
}
print(findPrimeNumberAfter(15))

//---------------- Второе задание ----------------

let start = CFAbsoluteTimeGetCurrent()

func showFirst100PrimesNumbers(size: Int = 100) -> [Int] {
    var array = [Int]()
    var n = 2
    while array.count != size {
        func isPrime() -> Bool {
            for i in 2..<n {
                if n % i == 0 {
                    return false
                }
            }
            return true
        }
        if isPrime() {
            array.append(n)
            n += 1
        } else {
            n += 1
        }
    }
    return array
}
//print(showFirst100PrimesNumbers(size: 1000))

let diff = CFAbsoluteTimeGetCurrent() - start
print("Took \(diff) seconds")

//---------------- Третье задание ----------------

let randomArray = (1...100).map{_ in Int(arc4random_uniform(100))}
print(randomArray)

func quicksort(_ a: [Int]) -> [Int] {
    guard a.count > 1 else { return a }
    
    let pivot = a[a.count/2]
    let less = a.filter { $0 < pivot }
    let equal = a.filter { $0 == pivot }
    let greater = a.filter { $0 > pivot }

    return quicksort(less) + equal + quicksort(greater)
}

let sortedArray = quicksort(randomArray)
print(sortedArray)

