//
//  ViewController.swift
//  Lesson2Osn
//
//  Created by Vladyslav Prosianyk on 27.04.2021.
//

import UIKit

protocol ViewControllerDelegate {
    func addAlertController(title: String?, message: String?, preferredStyle: UIAlertController.Style) -> UIAlertController
    
    var secretMail: String { get }
}

class ViewController: UIViewController, ViewControllerDelegate{
    
    var secretMail: String = "qwerty@abc.com"
    
    func addAlertController(title: String?, message: String?, preferredStyle: UIAlertController.Style) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: preferredStyle)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Закрыть", comment: "Default action"), style: .destructive, handler: { _ in
            NSLog("Alert has closed")
        }))
        self.present(alert, animated: true, completion: nil)
        return alert
    }
    
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    @IBOutlet weak var button3: UIButton!
    @IBOutlet weak var checkMail: UIButton!
    
    @IBAction func buttonPressed(_ sender: UIButton) {
        if sender == button1 {
            
            _ = addAlertController(title: "Поздравляю!", message: "Ты нажал на эту кнопку", preferredStyle: .alert)
            
        } else if sender == button2 {
            
            let alert2 = addAlertController(title: "Ничоси!!!", message: "вот и открылось это окно!", preferredStyle: .actionSheet)
            let alert = addAlertController(title: "Вот это да!", message: "Ты нажал также и на эту кнопку!", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "открыть другое окно", style: .default, handler: {_ in self.present(alert2, animated: true, completion: nil)}))
            
        } else if sender == button3 {
            
            let alert = addAlertController(title: "Ничоси!!!", message: "Ты ЧТО? Хочешь разблокировать почту?", preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: NSLocalizedString("тогда жми сюда", comment: ""), style: .default, handler: { _ in
                                            
                                            debugPrint("Алерт 3 закрыт")}))
            
        } else if sender == checkMail {
            
            let alert = UIAlertController(title: "Проверка почты", message: "Введите почту:", preferredStyle: .alert)
            alert.addTextField(configurationHandler: {(textField) in textField.placeholder = "почта"; textField.text = ""})
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
                let textField = alert?.textFields![0]
                let alertMail = UIAlertController(title: "Проверка почты", message: "", preferredStyle: .alert)
                if textField?.text != self.secretMail {
                    alertMail.message = "НЕВЕРНО!"
                    alertMail.addAction(UIAlertAction(title: "закрыть", style: .destructive, handler: {_ in debugPrint("Ошибочный ввод почты")}))
                    self.present(alertMail, animated: true, completion: nil)
                } else {
                    alertMail.message = "ТЫ НАПИСАЛ ВСЁ ПРАВИЛЬНО!"
                    alertMail.addAction(UIAlertAction(title: "закрыть", style: .destructive, handler: {_ in debugPrint("Ввод почты правильный")}))
                    self.present(alertMail, animated: true, completion: nil)
                }
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
}

