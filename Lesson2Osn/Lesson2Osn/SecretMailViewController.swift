//
//  SecretMailViewController.swift
//  Lesson2Osn
//
//  Created by Vladyslav Prosianyk on 29.04.2021.
//

import UIKit



class SecretMailViewController: UIViewController {
    
    var delegate: ViewControllerDelegate?
    
    @IBOutlet weak var lblSecretMail: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lblSecretMail.text = delegate?.secretMail
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
