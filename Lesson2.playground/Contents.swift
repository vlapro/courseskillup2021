import Foundation

var a = 24, b = 45, c = 7, d = 3       // написал в одну строку, чтобы не было в несколько строк

var x: Float = 34.223, y: Float = 4.5  // Float я использую, потому что он легче, чем Double

let mult = Int(x) * a                  // multiply constant
var dev = x * Float(a)                 // devision variable

debugPrint(a + d)                      // debugPrint
debugPrint(a - d)                      // debugPrint
debugPrint(a * d)                      // debugPrint
debugPrint(a / d)                      // debugPrint
debugPrint(a % d)                      // debugPrint
a += b                                 // присвоение нового значение для переменной 'a'
d -= c                                 // присвоение нового значение для переменной 'd'
x *= Float(a)                          // присвоение нового значение для переменной 'x' c переобразоваением переменной 'а' в Float
y /= x                                 // присвоение нового значение для переменной 'y'
b %= d                                 // присвоение нового значение для переменной 'b'


// Повышенная сложность -------------------------------------------------------------------------------------------

let sqrtA = sqrt(Double(a))
let sqrtB = sqrt(Double(b))
let sqrtC = sqrt(Double(c))


print("Корень квадратный с изпользованием функции sqrt(): \(sqrtA), \(sqrtB), \(sqrtC)")

