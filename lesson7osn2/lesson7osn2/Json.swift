//
//  Json.swift
//  lesson7osn2
//
//  Created by Vladyslav Prosianyk on 19.05.2021.
//

import Foundation

let jsonString = """
    {
      "PmQ-zNz6atI": {
        "user_name": "Jessie Shaw",
        "user_url": "https://unsplash.com/@jessieshawphoto?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit",
        "photo_url": "https://unsplash.com/photos/PmQ-zNz6atI?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit"
      },
      "XAuEWhpJ-Bc": {
        "user_name": "Patrick Jansen",
        "user_url": "https://unsplash.com/@patrickjjansen?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit",
        "photo_url": "https://unsplash.com/photos/XAuEWhpJ-Bc?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit"
      },
      "MT150s8Jx-Y": {
        "user_name": "Marija Zaric",
        "user_url": "https://unsplash.com/@simplicity?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit",
        "photo_url": "https://unsplash.com/photos/MT150s8Jx-Y?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit"
      },
      "x5hyhMBjR3M": {
        "user_name": "Content Pixie",
        "user_url": "https://unsplash.com/@contentpixie?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit",
        "photo_url": "https://unsplash.com/photos/x5hyhMBjR3M?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit"
      },
      "6IRO5IOQ_fE": {
        "user_name": "Matt Moloney",
        "user_url": "https://unsplash.com/@mattmoloney?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit",
        "photo_url": "https://unsplash.com/photos/6IRO5IOQ_fE?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit"
      },
      "7Ai0UwqqADs": {
        "user_name": "claudia lam",
        "user_url": "https://unsplash.com/@claudialam?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit",
        "photo_url": "https://unsplash.com/photos/7Ai0UwqqADs?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit"
      },
      "TtN03jUTA70": {
        "user_name": "Tanya Grypachevskaya",
        "user_url": "https://unsplash.com/@stilltane4ka?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit",
        "photo_url": "https://unsplash.com/photos/TtN03jUTA70?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit"
      },
      "QBBS_bBzY2g": {
        "user_name": "Jarek Šedý",
        "user_url": "https://unsplash.com/@jareksedy?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit",
        "photo_url": "https://unsplash.com/photos/QBBS_bBzY2g?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit"
      },
      "unSR-Rn90pI": {
        "user_name": "Apex 360",
        "user_url": "https://unsplash.com/@apex360_?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit",
        "photo_url": "https://unsplash.com/photos/unSR-Rn90pI?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit"
      },
      "nYKJStcr3z4": {
        "user_name": "Bianca Ackermann",
        "user_url": "https://unsplash.com/@biancablah?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit",
        "photo_url": "https://unsplash.com/photos/nYKJStcr3z4?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit"
      },
      "O96QaASSRcg": {
        "user_name": "Richard Horvath",
        "user_url": "https://unsplash.com/@orwhat?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit",
        "photo_url": "https://unsplash.com/photos/O96QaASSRcg?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit"
      },
      "ig-Z7HR8Knw": {
        "user_name": "Anna Scarfiello",
        "user_url": "https://unsplash.com/@little_anne?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit",
        "photo_url": "https://unsplash.com/photos/ig-Z7HR8Knw?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit"
      },
      "oamdoaY0qTs": {
        "user_name": "Mathilde Langevin",
        "user_url": "https://unsplash.com/@mathildelangevin?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit",
        "photo_url": "https://unsplash.com/photos/oamdoaY0qTs?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit"
      },
      "lolBVvVbkiI": {
        "user_name": "Brian Lundquist",
        "user_url": "https://unsplash.com/@bwl667?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit",
        "photo_url": "https://unsplash.com/photos/lolBVvVbkiI?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit"
      },
      "-heeB4Shku0": {
        "user_name": "Adrien Delforge",
        "user_url": "https://unsplash.com/@adriendlf?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit",
        "photo_url": "https://unsplash.com/photos/-heeB4Shku0?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit"
      },
      "03OY6tFZ1yU": {
        "user_name": "Wesley Armstrong",
        "user_url": "https://unsplash.com/@wesley_armstrong?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit",
        "photo_url": "https://unsplash.com/photos/03OY6tFZ1yU?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit"
      },
      "OaXn4QRu-QQ": {
        "user_name": "eberhard grossgasteiger",
        "user_url": "https://unsplash.com/@eberhardgross?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit",
        "photo_url": "https://unsplash.com/photos/OaXn4QRu-QQ?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit"
      },
      "ThkX-EuBxdA": {
        "user_name": "Andrea De Santis",
        "user_url": "https://unsplash.com/@santesson89?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit",
        "photo_url": "https://unsplash.com/photos/ThkX-EuBxdA?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit"
      },
      "JuXSfDkqTGo": {
        "user_name": "Marek Piwnicki",
        "user_url": "https://unsplash.com/@marekpiwnicki?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit",
        "photo_url": "https://unsplash.com/photos/JuXSfDkqTGo?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit"
      },
      "pQpvBO8Y5T8": {
        "user_name": "Jason Leung",
        "user_url": "https://unsplash.com/@ninjason?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit",
        "photo_url": "https://unsplash.com/photos/pQpvBO8Y5T8?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit"
      },
      "ma5cNyRLQ6k": {
        "user_name": "Jacob Smith",
        "user_url": "https://unsplash.com/@jsphotography?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit",
        "photo_url": "https://unsplash.com/photos/ma5cNyRLQ6k?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit"
      },
      "-dmVqeH3HIs": {
        "user_name": "David Becker",
        "user_url": "https://unsplash.com/@beckerworks?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit",
        "photo_url": "https://unsplash.com/photos/-dmVqeH3HIs?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit"
      },
      "dK0mLMsUHSY": {
        "user_name": "Michael Walk",
        "user_url": "https://unsplash.com/@walkthecreator?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit",
        "photo_url": "https://unsplash.com/photos/dK0mLMsUHSY?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit"
      },
      "zBSJbTwIWeg": {
        "user_name": "Lee Chinyama",
        "user_url": "https://unsplash.com/@leechinyama?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit",
        "photo_url": "https://unsplash.com/photos/zBSJbTwIWeg?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit"
      },
      "HkAy-PdSqeA": {
        "user_name": "Pawel Czerwinski",
        "user_url": "https://unsplash.com/@pawel_czerwinski?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit",
        "photo_url": "https://unsplash.com/photos/HkAy-PdSqeA?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit"
      },
      "_N3FWzS8SQg": {
        "user_name": "Samuel Regan-Asante",
        "user_url": "https://unsplash.com/@fkaregan?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit",
        "photo_url": "https://unsplash.com/photos/_N3FWzS8SQg?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit"
      },
      "-BnUKtXvBVQ": {
        "user_name": "Taylor Brandon",
        "user_url": "https://unsplash.com/@house_42?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit",
        "photo_url": "https://unsplash.com/photos/-BnUKtXvBVQ?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit"
      },
      "cP0_0McWGwo": {
        "user_name": "Cristian Newman",
        "user_url": "https://unsplash.com/@cristian_newman?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit",
        "photo_url": "https://unsplash.com/photos/cP0_0McWGwo?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit"
      },
      "tQcUEyZNazk": {
        "user_name": "Robin GAILLOT-DREVON",
        "user_url": "https://unsplash.com/@robingaillotdrevon?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit",
        "photo_url": "https://unsplash.com/photos/tQcUEyZNazk?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit"
      },
      "9f5ki8UvMy0": {
        "user_name": "Taylor Brandon",
        "user_url": "https://unsplash.com/@house_42?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit",
        "photo_url": "https://unsplash.com/photos/9f5ki8UvMy0?utm_source=unsample&utm_medium=referral&utm_campaign=api-credit"
      }
    }
    """
let jsonData = Data(jsonString.utf8)

