//
//  ImagePickerViewController.swift
//  lesson7osn2
//
//  Created by Vladyslav Prosianyk on 20.05.2021.
//

import UIKit

class ImagePickerViewController: UIViewController {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var autorName: UITextField!
    @IBOutlet weak var aboutPhoto: UITextField!
    @IBOutlet weak var photoURL: UITextField!
    
    let imagePicker = UIImagePickerController()
    
    @IBAction func addImage(_ sender: UIButton) {
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func addButtonPressed(_ sender: UIButton) {
        
    }
    
    @IBAction func cancelBtnPressed(_ sender: UIButton) {
        navigationController?.popToRootViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        // Do any additional setup after loading the view.
    }

}

extension ImagePickerViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            img.image = image
        }
        dismiss(animated: true, completion: nil)
    }
}
