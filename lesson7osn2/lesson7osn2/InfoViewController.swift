//
//  InfoViewController.swift
//  lesson7osn2
//
//  Created by Vladyslav Prosianyk on 19.05.2021.
//

import UIKit

class InfoViewController: UIViewController {
    
    static let identifier = "InfoViewController"

    @IBOutlet var tvInfo: UITextView!
    
    var info: String = ""
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        navigationController?.popToRootViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async {
            self.tvInfo.text = self.info
        }
    }

}
