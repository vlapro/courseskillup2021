//
//  Image.swift
//  lesson7osn2
//
//  Created by Vladyslav Prosianyk on 19.05.2021.
//

import UIKit

struct ImageValue: Codable {
    let userName: String
    let userURL: String
    let photoURL: String

    enum CodingKeys: String, CodingKey {
        case userName = "user_name"
        case userURL = "user_url"
        case photoURL = "photo_url"
    }
}

typealias Image = [String: ImageValue]

