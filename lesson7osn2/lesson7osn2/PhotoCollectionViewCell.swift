//
//  CollectionViewCell.swift
//  lesson7osn2
//
//  Created by Vladyslav Prosianyk on 19.05.2021.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var lText: UILabel!
        
    static let identifier = "PhotoCollectionViewCell"
    
    func configureCell(image: UIImage) {
        clipsToBounds = true
        layer.borderWidth = 1
        layer.borderColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3).cgColor
        layer.cornerRadius = 6
        backgroundColor = .black
        imageView.image = image
        imageView.layer.cornerRadius = 4
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onPressImage)))
        lText.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onPressText)))
    }
    
    func configureText(text: String) {
        lText.text = text
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.imageView.alpha = 1
        self.lText.alpha = 0
    }
    
    @objc func onPressImage() {
        UIView.animate(withDuration: 1) {
            self.imageView.alpha = 0
            self.lText.alpha = 1
        }
    }
    
    @objc func onPressText() {
        UIView.animate(withDuration: 1) {
            self.imageView.alpha = 1
            self.lText.alpha = 0
        }
    }
}
