//
//  PhotosCollectionViewController.swift
//  lesson7osn2
//
//  Created by Vladyslav Prosianyk on 19.05.2021.
//

import UIKit

class PhotosCollectionViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
        
    @IBOutlet weak var collectionView: UICollectionView?
    
    static let imageDataArray = try? JSONDecoder().decode(Image.self, from: jsonData)
    
    var keys = [String]()
    var userNameArray = [String]()
    var userURLArray = [String]()
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return keys.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PhotoCollectionViewCell.identifier, for: indexPath) as! PhotoCollectionViewCell
        let image = UIImage(named: keys[indexPath.item])
        cell.configureCell(image: image!)
        cell.configureText(text: configureData(keys[indexPath.item])!)
        return cell
    }
    
    func creatLayout() -> UICollectionViewFlowLayout {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: view.bounds.width - 20, height: view.bounds.height / 3)
        layout.minimumInteritemSpacing = 10
        layout.minimumLineSpacing = 10
        return layout
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        collectionView?.delegate = self
        collectionView?.dataSource = self
        
        collectionView?.collectionViewLayout = creatLayout()
        
        for (imageName, _) in PhotosCollectionViewController.imageDataArray! {
            keys.append(imageName)
        }
                
        collectionView?.register(UINib(nibName: PhotoCollectionViewCell.identifier, bundle: nil), forCellWithReuseIdentifier: PhotoCollectionViewCell.identifier)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        self.key = keys[indexPath.item]
    }
    
    var key: String?
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if let destination = segue.destination as? InfoViewController {
//            DispatchQueue.main.async {
//                destination.info = self.configureData(self.key!)!
//            }
//        }
//    }
//    
    func configureData(_ key: String) -> String? {
        let text = """
                    Name of autor: \(PhotosCollectionViewController.imageDataArray?["\(key)"]!.userName ?? "")
                    ********
                    URL of photo: \(PhotosCollectionViewController.imageDataArray?["\(key)"]!.photoURL ?? "")
                    ********
                    URL of autor: \(PhotosCollectionViewController.imageDataArray?["\(key)"]!.userURL ?? "")
                    """
        return text
    }
}



