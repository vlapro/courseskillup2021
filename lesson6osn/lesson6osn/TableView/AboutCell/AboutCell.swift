//
//  AboutCell.swift
//  lesson6osn
//
//  Created by Vladyslav Prosianyk on 11.05.2021.
//

import UIKit

class AboutCell: UITableViewCell, InfoDelegate {
    
    static let identifire = "AboutCell"

    @IBOutlet weak var tvAbout: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tvAbout.clipsToBounds = true
        tvAbout.layer.cornerRadius = 10
        tvAbout.layer.borderWidth = 1
        tvAbout.layer.borderColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
    }
    
    func checkInfo() -> Bool {
        if tvAbout.text!.isEmpty {
            return false
        } else {
            return true
        }
    }
    
    func getInfo() -> String {
        let text = """
        About me: \(tvAbout.text ?? "")\n
        """
        return text
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
