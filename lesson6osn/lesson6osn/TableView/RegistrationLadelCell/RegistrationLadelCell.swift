//
//  RegistrationLadelCell.swift
//  lesson6osn
//
//  Created by Vladyslav Prosianyk on 11.05.2021.
//

import UIKit

class RegistrationLadelCell: UITableViewCell {
    
    static let identifire = "RegistrationLadelCell"

    @IBOutlet weak var viewWithLbl: UIView!
    @IBOutlet weak var lRegistration: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewWithLbl.clipsToBounds = true
        viewWithLbl.layer.borderWidth = 2
        viewWithLbl.layer.borderColor = UIColor.gray.cgColor
        viewWithLbl.layer.cornerRadius = 10
        lRegistration.text = "Registration"
        lRegistration.font = UIFont(name: "Helvetica Neue Bold", size: 25)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
