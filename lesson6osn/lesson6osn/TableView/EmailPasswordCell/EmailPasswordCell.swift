//
//  EmailPasswordCell.swift
//  lesson6osn
//
//  Created by Vladyslav Prosianyk on 11.05.2021.
//

import UIKit

class EmailPasswordCell: UITableViewCell, InfoDelegate {
    
    static let identifire = "EmailPasswordCell"

    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func checkInfo() -> Bool {
        if tfEmail.text!.isEmpty || tfPassword.text!.isEmpty {
            return false
        } else {
            return true
        }
    }
    
    func getInfo() -> String {
        let text = """
        Email: \(tfEmail.text ?? "")
        Password: \(tfPassword.text ?? "")\n
        """
        return text
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
