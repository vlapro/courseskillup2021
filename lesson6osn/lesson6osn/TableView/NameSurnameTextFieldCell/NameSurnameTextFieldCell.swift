//
//  NameSurnameTextFieldCell.swift
//  lesson6osn
//
//  Created by Vladyslav Prosianyk on 11.05.2021.
//

import UIKit

class NameSurnameTextFieldCell: UITableViewCell, InfoDelegate {
    
    static let identifire = "NameSurnameTextFieldCell"

    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var tfSurname: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func checkInfo() -> Bool {
        if tfName.text!.isEmpty || tfSurname.text!.isEmpty {
            return false
        } else {
            return true
        }
    }
    
    func getInfo() -> String {
        let text = """
        Name: \(tfName.text ?? "")
        Surname: \(tfSurname.text ?? "")\n
        """
        return text
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
