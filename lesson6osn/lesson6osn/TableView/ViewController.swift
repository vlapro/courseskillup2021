//
//  ViewController.swift
//  lesson6osn
//
//  Created by Vladyslav Prosianyk on 10.05.2021.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var vButtonView: UIView!
    @IBOutlet weak var bButton: UIButton!
    
    @IBAction func saveButtonPressed(_ sender: UIButton) {
        let getAllCell = self.getAllCells()
        var text = ""
        let vc = storyboard?.instantiateViewController(identifier: UserScreenViewController.identifire) as! UserScreenViewController
        let setTextDelegate: SetTextDelegate? = vc.self
        for i in getAllCell {
            if let cell = i as? InfoDelegate {
                if cell.checkInfo() == false {
                    changeButtonColor(cell.checkInfo())
                    break
                } else {
                    changeButtonColor(cell.checkInfo())
                    let returnedText = cell.getInfo()
                    text.append(returnedText)
                }
            }
        }
        navigationController?.pushViewController(vc, animated: true)
        setTextDelegate?.setText(text: text)
    }
    
    private let tableView: UITableView = {
        let table = UITableView()
        
        table.register(UINib(nibName: NameSurnameTextFieldCell.identifire, bundle: nil),
                       forCellReuseIdentifier: NameSurnameTextFieldCell.identifire)
        table.register(UINib(nibName: EmailPasswordCell.identifire, bundle: nil),
                       forCellReuseIdentifier: EmailPasswordCell.identifire)
        table.register(UINib(nibName: RegistrationLadelCell.identifire, bundle: nil),
                       forCellReuseIdentifier: RegistrationLadelCell.identifire)
        table.register(UINib(nibName: CityPhoneCell.identifire, bundle: nil),
                       forCellReuseIdentifier: CityPhoneCell.identifire)
        table.register(UINib(nibName: AboutCell.identifire, bundle: nil),
                       forCellReuseIdentifier: AboutCell.identifire)
        
        return table
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(tableView)
        tableView.separatorStyle = .none
        tableView.dataSource = self
        tableView.delegate = self
        setButton()
        let tap = UITapGestureRecognizer(target: self, action: #selector(ViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = view.bounds.inset(by: UIEdgeInsets(top: .zero, left: 0.0, bottom: vButtonView.frame.height, right: .zero))
    }
    
}

extension ViewController {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellRegLbl =  tableView.dequeueReusableCell(withIdentifier: RegistrationLadelCell.identifire,
                                                        for: indexPath) as! RegistrationLadelCell
        let cellNmSrnm =  tableView.dequeueReusableCell(withIdentifier: NameSurnameTextFieldCell.identifire,
                                                        for: indexPath) as! NameSurnameTextFieldCell
        let cellEmlPsw =  tableView.dequeueReusableCell(withIdentifier: EmailPasswordCell.identifire,
                                                        for: indexPath) as! EmailPasswordCell
        let cellCtPhn =   tableView.dequeueReusableCell(withIdentifier: CityPhoneCell.identifire,
                                                        for: indexPath) as! CityPhoneCell
        let cellAbt =     tableView.dequeueReusableCell(withIdentifier: AboutCell.identifire,
                                                        for: indexPath) as! AboutCell

        if indexPath.row == 0 {
            return cellRegLbl
        } else if indexPath.row == 1 {
            return cellNmSrnm
        } else if indexPath.row == 2 {
            return cellEmlPsw
        } else if indexPath.row == 3 {
            return cellCtPhn
        } else if indexPath.row == 4 {
            return cellAbt
        } else {
            return UITableViewCell()
        }
    }
}


extension ViewController {
    func setButton() {
        bButton.titleLabel?.font = UIFont(name: "Helvetica Neue Bold", size: 20)
        bButton.titleLabel?.tintColor = .darkText
        bButton.setTitle("Save", for: .normal)
        bButton.setTitle("Save", for: .selected)
        bButton.backgroundColor = .blue
        bButton.clipsToBounds = true
        bButton.layer.cornerRadius = 10
    }
    
    func changeButtonColor(_ isUnblock: Bool) {
        bButton.backgroundColor = isUnblock ? .blue : .red
    }
    

    
    func getAllCells() -> [UITableViewCell] {
        
        var cells = [UITableViewCell]()
        
        for i in 0...tableView.numberOfSections-1 {
            for j in 0...tableView.numberOfRows(inSection: i)-1 {
                if let cell = tableView.cellForRow(at: NSIndexPath(row: j, section: i) as IndexPath) {
                    cells.append(cell)
                }
            }
        }
        return cells
    }
}
