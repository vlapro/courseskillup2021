//
//  CityPhoneCell.swift
//  lesson6osn
//
//  Created by Vladyslav Prosianyk on 11.05.2021.
//

import UIKit

class CityPhoneCell: UITableViewCell, InfoDelegate {
    
    static let identifire = "CityPhoneCell"
    
    @IBOutlet weak var tfPhone: UITextField!
    @IBOutlet weak var tfCity: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func getInfo() -> String {
        let text = """
        Phone number: \(tfPhone.text ?? "")
        City: \(tfCity.text ?? "")\n
        """
        return text
    }
    
    func checkInfo() -> Bool {
        if tfPhone.text!.isEmpty || tfCity.text!.isEmpty {
            return false
        } else {
            return true
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
