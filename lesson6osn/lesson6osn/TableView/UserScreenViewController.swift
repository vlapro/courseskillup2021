//
//  UserScreenViewController.swift
//  lesson6osn
//
//  Created by Vladyslav Prosianyk on 11.05.2021.
//

import UIKit

class UserScreenViewController: UIViewController, SetTextDelegate {
    
    static let identifire = "UserScreenViewController"

    @IBOutlet weak var tvAllInfo: UITextView!
    
    @IBAction func editButtonPressed(_ sender: UIButton) {
        navigationController?.popToRootViewController(animated: true)
    }
    func setText(text: String) {
        DispatchQueue.main.async {
            self.tvAllInfo?.text = text
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

}
