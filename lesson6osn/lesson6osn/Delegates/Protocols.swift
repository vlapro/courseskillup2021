//
//  Protocols.swift
//  lesson6osn
//
//  Created by Vladyslav Prosianyk on 11.05.2021.
//

import Foundation

protocol InfoDelegate {
    func checkInfo() -> Bool
    func getInfo() -> String
}

protocol SetTextDelegate {
    func setText(text: String)
}

