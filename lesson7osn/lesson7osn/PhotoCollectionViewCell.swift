//
//  PhotoCollectionViewCell.swift
//  lesson7osn
//
//  Created by Vladyslav Prosianyk on 14.05.2021.
//

import UIKit

protocol CustomDelegate {
    func configureData(_ key: String) -> String?
}


class PhotoCollectionViewCell: UICollectionViewCell, UIGestureRecognizerDelegate {
    
    static let identifier = "PhotoCollectionViewCell"
    var delegate: CustomDelegate?
    var key: String?
    var index: Int?
    
    var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.isUserInteractionEnabled = true
        return imageView
    }()

    func update() {
        
        let viewWithText: UIView = {
            for item in imageView.subviews {
                item.removeFromSuperview()
            }
            let view = UIView()
            let text = UITextView(frame: CGRect(x: 30,
                                                y: 30,
                                                width: imageView.bounds.size.width - 60,
                                                height: imageView.bounds.size.height - 60))
            
            imageView.addBlurEffect()
            text.allowsEditingTextAttributes = false
            text.text = delegate?.configureData(key ?? "")
            text.font = UIFont(name: "Helvetica Neue Thin", size: 15)
            text.textColor = .white
            text.backgroundColor = .clear
            view.addSubview(text)
            return view
        }()
        
        if contentView.contains(viewWithText) {
            print("hallo")
            imageView.delete(subviews)
        } else {
            imageView.addSubview(viewWithText)
            
            
            
        }
        
        
    }
    

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contentView.addSubview(imageView)
        contentView.clipsToBounds = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        imageView.frame = contentView.bounds
    }
    
    func configureWith(image: UIImage) {
        imageView.image = image
        
    }
    
}
