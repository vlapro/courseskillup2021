//
//  PhotosCollectionViewController.swift
//  lesson7osn
//
//  Created by Vladyslav Prosianyk on 14.05.2021.
//

import UIKit

class PhotosCollectionViewController: UIViewController {
    
    static let identifier = "PhotosCollectionViewController"
    
    let collectionView: UICollectionView? = UICollectionView(frame: CGRect(), collectionViewLayout: UICollectionViewLayout())
    
    static let imageDataArray = try? JSONDecoder().decode(Image.self, from: jsonData)
    
    var myImageArray = [String]()
    var userNameArray = [String]()
    var userURLArray = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: view.frame.size.width - 20, height: view.frame.size.height / 3)
        layout.minimumLineSpacing = 10
        layout.minimumInteritemSpacing = 10
        
        collectionView?.collectionViewLayout = layout
        
        collectionView?.delegate = self
        collectionView?.dataSource = self
        
        collectionView?.register(PhotoCollectionViewCell.self, forCellWithReuseIdentifier: PhotoCollectionViewCell.identifier)
        
        for (imageName, _) in PhotosCollectionViewController.imageDataArray! {
            myImageArray.append(imageName)
        }
        
        guard let collectionView = collectionView else {
            return
        }
        view.addSubview(collectionView)
        collectionView.frame = view.bounds
        
    }
    
}



extension PhotosCollectionViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if view.subviews.isEmpty {
            print("YES")
        } else {
            let cell = collectionView.cellForItem(at: indexPath) as! PhotoCollectionViewCell
            cell.update()
        }
        collectionView.deselectItem(at: indexPath, animated: true)
        
        print("tap \(indexPath)")
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return myImageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PhotoCollectionViewCell.identifier, for: indexPath) as! PhotoCollectionViewCell
        cell.clipsToBounds = true
        cell.layer.borderWidth = 1
        cell.layer.borderColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3).cgColor
        cell.layer.cornerRadius = 6
        cell.imageView.image = UIImage(named: myImageArray[indexPath.item])
        cell.delegate = self
        cell.key = myImageArray[indexPath.item]
        cell.index = indexPath.item
        cell.imageView.layer.cornerRadius = 4
        return cell
    }
}

extension UIView {
    
    func addBlurEffect() {
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(blurEffectView)
    }
    
    func removeBlurEffect() {
        let blurredEffectViews = self.subviews.filter{$0 is UIVisualEffectView}
        blurredEffectViews.forEach{ blurView in
            blurView.removeFromSuperview()
        }
    }
}
